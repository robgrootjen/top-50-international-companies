import requests
from bs4 import BeautifulSoup
import csv

##################################################################################################

#Source
result1 = requests.get('https://en.wikipedia.org/wiki/List_of_largest_companies_by_revenue')

###################################################################################################

#Save source in variable
src1 = result1.content

####################################################################################################

#Activate soup
soup = BeautifulSoup(src1,'lxml')

####################################################################################################

#Look for table and save in CSV

table = soup.find('table')
with open('top50.csv','w',newline='') as f:
    writer = csv.writer(f)
    for tr in table('tr'):
        row = [t.get_text(strip=True) for t in tr(['td','th'])]
        cell1 = row[0]
        cell2 = row[1]
        cell3 = row[2]
        cell4 = row[3].replace(',','').replace('$','')
        cell5 = row[4].replace(',','').replace('$','')
        cell6 = row[5].replace(',','').replace('$','')
        cell7 = row[6]
        row = [cell1,cell2,cell3,cell4,cell5,cell6,cell7]
        writer.writerow(row)

######################################################################################################