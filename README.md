The datapackage shows yearly revenue, profit and employees of the top 50 international companies.

## Data
The data is sourced from: 
* https://en.wikipedia.org/wiki/List_of_largest_companies_by_revenue

The repo includes:
* datapackage.json
* top50.csv
* process.py

--------------------------------------------------------------------------------------------------------------------------------

## Preparation

Requires:
1. Python
2. Visualstudio / Jupyter notebook / or any platform that works with python.

***top50.csv*** 
* The CSV Data has 7 colummns. 
    * Rank
    * Name
    * Industry
    * Revenue(USD millions)
    * Profit(USD millions)
    * Employees
    * Country
    
***datapackage.json***
* The json file has all the information from the two csv files, licence, author and includes 3 line graphs.

***process.py***
* The script will scrape the whole table from - https://en.wikipedia.org/wiki/List_of_largest_companies_by_revenue
    
***Instructions:***
* Copy process.py script in the "Process" folder.
* Open in jupyter notebook, python shell, VS code, or any preferred platform.
* Run the code
* 3 CSV files will be saved in document where your terminal is at the moment.
----------------------------------------------------------------------------------------------------------------------------------------

## Licence
This Data Package is made available under the Public Domain Dedication and License v1.0 
